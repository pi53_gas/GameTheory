var container = document.getElementById('container');
var generateBtn = document.getElementById('generateBtn');
var containerBtn = document.getElementById('buttons');
var matrixContainer = document.getElementById('matrix');
var reactionMatrix = document.getElementById('reactionMatrix');
var reactionFnContainer = document.getElementById('reactionFnContainer');
var resultFnContainer = document.getElementById('resultFnContainer');
var binaryMatrix = document.getElementById('binaryMatrix');
var binaryMatrixPl1 = document.getElementById('binaryMatrixPl1');
var binaryMatrixPl2 = document.getElementById('binaryMatrixPl2');
var formulaContainer = document.getElementById('formula');
var formulaPl1 = document.getElementById('formulaPl1');
var formulaPl2 = document.getElementById('formulaPl2');
var solveMatrix = document.getElementById('solveMatrix');
var imageContainer = document.getElementById('imageContainer');

var btnPlayer1 = [];
var btnPlayer2 = [];
var matrixPlayer1 = [];
var matrixPlayer2 = [];

var letter = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

//Генерация псевдорандомных чисел
function randomInteger(min, max) {
    var random = min - 0.5 + Math.random() * (max - min + 1);
    random = Math.round(random);
    return random;
}

//Генерация цвета в rgb
function randomColor() {
    var random = 1 - 0.5 + Math.random() * (255 - 1 + 1);
    random = Math.round(random);
    return random;
}
//Массив первого игрока
var player1Array = [];
//Массив второго игрока
var player2Array = [];

//Заполнение массива первого игрока
function generateArrayPlayer1(array, strategyPlayer1, min, max) {
    for(var i = 0; i < strategyPlayer1*strategyPlayer1; i++){
        array[i] = randomInteger(min, max);
    }
    return array;
}

//Заполнение массива второго игрока
function generateArrayPlayer2(array, strategyPlayer2, min, max) {
    for(var i = 0; i < strategyPlayer2; i++){
        array.push(randomInteger(min, max));
    }
    return array;
}

//Генерация кнопок для первого игрока
function generateBtnPlayer(array, container, i) {
    var btn = document.createElement('button');
    container.appendChild(btn);
    btn.innerText = letter[i];
    btn.classList.add('button', 'btn-primary', 'btn');
    array[i] = btn;
}

//Задаем id для каждой кнопки первого игрока
function setIdBtnPlayer1(btn, i) {
    btn.setAttribute("id", "btnPlayerFirst" + i);
}

//Задаем id для каждой кнопки второго игрока
function setIdBtnPlayer2(btn, i) {
    btn.setAttribute("id", "btnPlayerSecond" + i);
}

function setIdBinaryBtnPlayer1(btn, i) {
    btn.setAttribute("id", "btnBinaryPlayerFirst" + i);
}
function setIdBinaryBtnPlayer2(btn, i) {
    btn.setAttribute("id", "btnBinaryPlayerSecond" + i);
}
function setIdBinary2BtnPlayer1(btn, i) {
    btn.setAttribute("id", "btnBinary2PlayerFirst" + i);
}
function setIdBinary2BtnPlayer2(btn, i) {
    btn.setAttribute("id", "btnBinary2PlayerSecond" + i);
}
//Функция создания кнопки
function createBtn(text) {
    var btn = document.createElement('button');
    containerBtn.appendChild(btn);
    btn.classList.add('btn-danger', 'btn', 'matrix-btn');
    btn.innerText = text;
    return btn;
}
//Генерация матрицы первого игрока
function generateMatrixPlayer1(matrixPlayer1, strategyPlayer1) {
    for (var i = 0; i < strategyPlayer1; i++) {
        matrixPlayer1[i] = [];
        for (var j = 0; j < strategyPlayer1; j++) {
            matrixPlayer1[i][j] = player1Array[randomInteger(0, strategyPlayer1-1)];
        }
    }
    return matrixPlayer1;
}
//Генерация матрицы второго игрока
function generateMatrixPlayer2(matrixPlayer2, strategyPlayer2) {
    for (var i = 0; i < strategyPlayer2; i++) {
        matrixPlayer2[i] = [];
        for (var j = 0; j < strategyPlayer2; j++) {
            matrixPlayer2[i][j] = player2Array[randomInteger(0, strategyPlayer2-1)];
        }
    }
    return matrixPlayer2;
}



var strategyPlayer1, strategyPlayer2, minRange, maxRange;
function generateStrategy() {
    strategyPlayer1 = +document.getElementById('strategyPlayer1').value;
    strategyPlayer2 = +document.getElementById('strategyPlayer2').value;

    minRange = +document.getElementById('rangeMinPlayer').value;
    maxRange = +document.getElementById('rangeMaxPlayer').value;

    player1Array = generateArrayPlayer1(player1Array, strategyPlayer1, minRange, maxRange);
    player2Array = generateArrayPlayer2(player2Array, strategyPlayer2, minRange, maxRange);

    matrixPlayer1 = generateMatrixPlayer1(matrixPlayer1, strategyPlayer1);
    matrixPlayer2 = generateMatrixPlayer2(matrixPlayer2, strategyPlayer2);
}
var stopBtn, binaryMatrixBtn, solveBtn, functionBtn, solveFunctionBtn;
function createMatrixBtn() {
    generateBtn.disabled = true;

    //Генерация стратегий
    generateStrategy();

    //Кнопка для генерации матрицы
    var matrixBtn = createBtn("Матриця");
    matrixBtn.addEventListener('click', createMatrix);

    //Кнопка создания бинарных матриц
    binaryMatrixBtn = createBtn("Бінарна матриця");
    binaryMatrixBtn.addEventListener('click', createBinaryMatrix);

    //Кнопка, которая блокирует кнопки
    stopBtn = createBtn("Розв'язати");

    //Кнопка для автоматического решений матрицы
    solveBtn = createBtn("Розв'язати автоматично");

    //Кнопка генерации функций реакции
    functionBtn = createBtn("Функції реакцій");
    functionBtn.addEventListener('click', reactionFunction);

    //Кнопка автоматического решений функций реакций
    solveFunctionBtn = createBtn("Розв'язати автоматично функції реакцій");
    solveFunctionBtn.disabled = true;
}

function createTable(strategyPlayer1, strategyPlayer2, matrixPlayer1, matrixPlayer2) {
    var table = document.createElement("table");
    for (var y = 0; y <= strategyPlayer1; y++) {
        var tr = document.createElement("tr");
        for (var x = 0; x <= strategyPlayer2; x++) {
            var td = document.createElement("td");
            var input = document.createElement('input');
            input.setAttribute("type", "text");
            tr.appendChild(td);
            td.appendChild(input);
            //input.value = player1Array[randomValuePl1[y]] + ' ; ' + player2Array[randomInteger(0,4)];
            input.classList.add('value');
            if(y !== strategyPlayer1 && x !== strategyPlayer2) {
                input.value = matrixPlayer1[y][x] + ';' + matrixPlayer2[y][x];
                input.classList.add('value');
            }
            if(y == strategyPlayer1){
                td.innerText = '';
                generateBtnPlayer(btnPlayer2, td, x);
                setIdBtnPlayer2(btnPlayer2[x], x);
            }
            if(x == strategyPlayer2){
                td.innerText = '';
                generateBtnPlayer(btnPlayer1, td, y);
                setIdBtnPlayer1(btnPlayer1[y], y);
            }
            if(x == strategyPlayer2 && y == strategyPlayer1)
                td.innerHTML = '';
        }
        table.appendChild(tr);
    }
    return table;
}

function createNewTable(strategyPlayer1, strategyPlayer2, matrixPlayer1, matrixPlayer2) {
    var table = document.createElement("table");
    for (var y = 0; y < strategyPlayer1; y++) {
        var tr = document.createElement("tr");
        for (var x = 0; x < strategyPlayer2; x++) {
            var td = document.createElement("td");
            tr.appendChild(td);
            td.innerText = matrixPlayer1[y][x] + ' ; ' + matrixPlayer2[y][x];
            td.classList.add('table-td');
        }
        table.appendChild(tr);
    }
    return table;
}
function createNewBinaryTable(strategyPlayer1, strategyPlayer2, value) {
    var table = document.createElement("table");
    for (var y = 0; y < strategyPlayer1; y++) {
        var tr = document.createElement("tr");
        for (var x = 0; x < strategyPlayer2; x++) {
            var td = document.createElement("td");
            tr.appendChild(td);
            td.innerText = value[y][x];//matrixPlayer1[y][x] + ' ; ' + matrixPlayer2[y][x];
            td.classList.add('table-td');
        }
        table.appendChild(tr);
    }
    return table;
}

function reactionStrategy(y, x, strategyPlayer1, strategyPlayer2, strategyArray, td){
    if(y !== strategyPlayer1 && x !== strategyPlayer2) {
        td.innerText = strategyArray[y][x];
    }
    if(y == strategyPlayer1){
        td.innerText = letter[x];
        td.style.background = 'rgba(8, 249, 247, 0.5)';
    }
    if(x == strategyPlayer2){
        td.innerText = letter[y];
        td.style.background = 'rgba(8, 249, 247, 0.5)';
    }
    if(x == strategyPlayer2 && y == strategyPlayer1) {
        td.innerHTML = '';
        td.style.background = 'transparent';
    }
}
function reactionStrategySolveMatrix(y, x, strategyPlayer1, strategyPlayer2, strategyArray1, strategyArray2, td){
    if(y !== strategyPlayer1 && x !== strategyPlayer2) {
        td.innerText = matrixPlayer1[y][x] + ' ; ' + matrixPlayer2[y][x];
    }
    if(y == strategyPlayer1){
        td.innerText = letter[x];
        td.style.background = 'rgba(8, 249, 247, 0.5)';
    }
    if(x == strategyPlayer2){
        td.innerText = letter[y];
        td.style.background = 'rgba(8, 249, 247, 0.5)';
    }
    if(x == strategyPlayer2 && y == strategyPlayer1) {
        td.innerHTML = '';
        td.style.background = 'transparent';
    }
}
function createBinaryMatrixPlayer1(strategyPlayer1, strategyPlayer2, value) {
    var table = document.createElement("table");
    for (var y = 0; y <= strategyPlayer1; y++) {
        var tr = document.createElement("tr");
        for (var x = 0; x <= strategyPlayer2; x++) {
            var td = document.createElement("td");
            tr.appendChild(td);
            var input = document.createElement('input');
            input.setAttribute("type", "text");
            tr.appendChild(td);
            td.appendChild(input);
            input.classList.add('value');
            if(y !== strategyPlayer1 && x !== strategyPlayer2) {
                input.value = value[y][x];
                input.classList.add('value');
            }
            if(y == strategyPlayer1){
                td.innerText = '';
                generateBtnPlayer(btnPlayer2, td, x);
                setIdBinaryBtnPlayer2(btnPlayer2[x], x);
            }
            if(x == strategyPlayer2){
                td.innerText = '';
                generateBtnPlayer(btnPlayer1, td, y);
                setIdBinaryBtnPlayer1(btnPlayer1[y], y);
            }
            if(x == strategyPlayer2 && y == strategyPlayer1)
                td.innerHTML = '';
        }
        table.appendChild(tr);
    }
    return table;
}
function createBinaryMatrixPlayer2(strategyPlayer1, strategyPlayer2, value) {
    var table = document.createElement("table");
    for (var y = 0; y <= strategyPlayer1; y++) {
        var tr = document.createElement("tr");
        for (var x = 0; x <= strategyPlayer2; x++) {
            var td = document.createElement("td");
            tr.appendChild(td);
            var input = document.createElement('input');
            input.setAttribute("type", "text");
            tr.appendChild(td);
            td.appendChild(input);
            input.classList.add('value');
            if(y !== strategyPlayer1 && x !== strategyPlayer2) {
                input.value = value[y][x];
                input.classList.add('value');
            }
            if(y == strategyPlayer1){
                td.innerText = '';
                generateBtnPlayer(btnPlayer2, td, x);
                setIdBinary2BtnPlayer2(btnPlayer2[x], x);
            }
            if(x == strategyPlayer2){
                td.innerText = '';
                generateBtnPlayer(btnPlayer1, td, y);
                setIdBinary2BtnPlayer1(btnPlayer1[y], y);
            }
            if(x == strategyPlayer2 && y == strategyPlayer1)
                td.innerHTML = '';
        }
        table.appendChild(tr);
    }
    return table;
}
function createReactionFunctionPl1(strategyPlayer1, strategyPlayer2, strategyArray) {
    var table = document.createElement("table");
    for (var y = 0; y <= strategyPlayer1; y++) {
        var tr = document.createElement("tr");
        for (var x = 0; x <= strategyPlayer2; x++) {
            var td = document.createElement("td");
            tr.appendChild(td);
            td.classList.add('table-td');
            reactionStrategy(y, x, strategyPlayer1, strategyPlayer2, strategyArray, td);
        }
        table.appendChild(tr);
    }
    return table;
}

function createReactionFunctionPl2(strategyPlayer1, strategyPlayer2, strategyArray) {
    var table = document.createElement("table");
    for (var y = 0; y <= strategyPlayer1; y++) {
        var tr = document.createElement("tr");
        for (var x = 0; x <= strategyPlayer2; x++) {
            var td = document.createElement("td");
            tr.appendChild(td);
            td.classList.add('table-td');
            reactionStrategy(y, x, strategyPlayer1, strategyPlayer2, strategyArray, td);
        }
        table.appendChild(tr);
    }
    return table;
}
function createSolveTable(strategyPlayer1, strategyPlayer2, matrixPlayer1, matrixPlayer2) {
    var table = document.createElement("table");
    for (var y = 0; y <= strategyPlayer1; y++) {
        var tr = document.createElement("tr");
        for (var x = 0; x <= strategyPlayer2; x++) {
            var td = document.createElement("td");
            tr.appendChild(td);
            td.classList.add('table-td');
            reactionStrategySolveMatrix(y, x, strategyPlayer1, strategyPlayer2, matrixPlayer1, matrixPlayer2, td);
        }
        table.appendChild(tr);
    }
    return table;
}
var table;
function createText(text, css, container) {
    var title = document.createElement("span");
    container.appendChild(title);
    title.innerText = text;
    title.classList.add(css);
}

function updateMatrix() {
    
}
function createMatrix() {

    solveFunctionBtn.disabled = true;
    reactionFnContainer.innerHTML = '';
    resultFnContainer.innerHTML = '';
    binaryMatrixPl1.innerHTML = '';
    binaryMatrixPl2.innerHTML = '';
    formulaContainer.innerHTML = '';

    table = createTable(strategyPlayer1, strategyPlayer2, matrixPlayer1, matrixPlayer2);
    matrixContainer.appendChild(table);

    var count = 1;
    function deleteColumn(value) {
        for (var i = 0; i < table.rows.length; i++) {
            for (var j = 0; j < table.rows[i].cells.length; j++) {
                if (j == value)
                    table.rows[i].cells[j].style.display = "none";
            }
        }
        createText(count + ' хід (Видалена стратегія Player2 - ' + btnPlayer2[value].innerText + ')', 'text', matrixContainer);
        var newTable =  createNewTable(strategyPlayer1, strategyPlayer2, matrixPlayer1, matrixPlayer2);
        matrixContainer.appendChild(newTable);
        count++;
        var color = 'rgba(' + randomColor() + ',' + randomColor() + ',' + randomColor() + ',0.7)';
        for (var i = 0; i < newTable.rows.length; i++) {
            for (var j = 0; j < newTable.rows[i].cells.length; j++) {
                if (j == value)
                    newTable.rows[i].cells[j].style.background = color;
            }
        }
    }

    //createText("Історія\n", 'title');
    function deleteRow(i, row) {
        var index = row.parentNode.parentNode.rowIndex;
        createText(count + ' хід (Видалена стратегія Player1 - ' + btnPlayer1[i].innerText + ')', 'text', matrixContainer);
        var newTable =  createNewTable(strategyPlayer1, strategyPlayer2, matrixPlayer1, matrixPlayer2);
        matrixContainer.appendChild(newTable);
        newTable.rows[i].style.background = 'rgba(' + randomColor() + ',' + randomColor() + ',' + randomColor() + ',0.7)';
        table.deleteRow(index);
        count++;
    }

     function deleteElement(id, value) {
         document.getElementById(id).onclick = function () {
             deleteRow(value, this);
         };
     }

    function deleteElementColumn(id, value) {
        document.getElementById(id).onclick = function () {
            deleteColumn(value);
        };
    }

    stopBtn.onclick = function () {
        for(var i = 0; i < strategyPlayer1; i++)
            btnPlayer1[i].disabled = true;
        for(var i = 0; i < strategyPlayer2; i++)
            btnPlayer2[i].disabled = true;
    };

    for(var i = 0; i < table.rows.length; i++){ //Проход по таблице, задается главная таблица - начальная
        for(var j = 0; j < table.rows[i].cells.length; j++){
            table.rows[i].cells[j].onkeyup = function (ev) {
                var rowIndex = this.parentElement.rowIndex;
                var cellIndex = this.cellIndex;
                var text = ev.target.value;
                var elem = text.split(';');
                matrixPlayer1[rowIndex][cellIndex] = elem[0]; // массив первого игрока в ввиде матрицы
                matrixPlayer2[rowIndex][cellIndex] = elem[1]; // массив второго игрока в ввиде матриццы
            };
        }
    }
    function solveStrategyMatrix(solveTable){

    }
    solveBtn.onclick = function () {
        var btn = document.createElement('button');
        containerBtn.appendChild(btn);
        btn.innerText = 'Видалити';
        //solveBtn.innerText = 'Видалити';
        var solveTable = createSolveTable(strategyPlayer1, strategyPlayer2, matrixPlayer1, matrixPlayer2);
        solveMatrix.appendChild(solveTable);
        btn.onclick = function () {
            var isMore = false;
            var index = 0;
            var text = document.createElement('span');
            solveMatrix.appendChild(text);
            for(var i = 0; i < matrixPlayer1.length; i++){
                for(var j = 0; j < matrixPlayer1.length; j++){
                    if(matrixPlayer1[i][j] > matrixPlayer1[i+1][j] || matrixPlayer1[i][j] < matrixPlayer1[i+1][j]){
                        isMore = true;
                        index = i;

                    }
                    else {
                        solveBtn.disabled = 'true';
                        text.innerText = 'Нема ходів';
                    }
                }
            }

            if(isMore === true){
                var solveTable2 = createSolveTable(strategyPlayer1, strategyPlayer2, matrixPlayer1, matrixPlayer2);
                solveMatrix.appendChild(solveTable2);
                solveTable2.deleteRow(index);
            }
        }

    };

     deleteElement('btnPlayerFirst0', 0);
     deleteElementColumn('btnPlayerSecond0', 0);
     deleteElement('btnPlayerFirst1', 1);
     deleteElementColumn('btnPlayerSecond1', 1);
     deleteElement('btnPlayerFirst2', 2);
     deleteElementColumn('btnPlayerSecond2', 2);
     deleteElement('btnPlayerFirst3', 3);
     deleteElementColumn('btnPlayerSecond3', 3);
     deleteElement('btnPlayerFirst4', 4);
     deleteElementColumn('btnPlayerSecond4', 4);
     deleteElement('btnPlayerFirst5', 5);
     deleteElementColumn('btnPlayerSecond5', 5);
     deleteElement('btnPlayerFirst6', 6);
     deleteElementColumn('btnPlayerSecond6', 6);
     deleteElement('btnPlayerFirst7', 7);
     deleteElementColumn('btnPlayerSecond7', 7);
     deleteElement('btnPlayerFirst8', 8);
     deleteElementColumn('btnPlayerSecond8', 8);
     deleteElement('btnPlayerFirst9', 9);
     deleteElementColumn('btnPlayerSecond9', 9);

}



function reactionFunction() {

    matrixContainer.innerHTML = '';
    binaryMatrixPl1.innerHTML = '';
    binaryMatrixPl2.innerHTML = '';
    solveFunctionBtn.disabled = false;
    var selectedTd;
    function highlight(node) {
        if (selectedTd) {
            selectedTd.classList.remove('highlight');
        }
        selectedTd = node;
        selectedTd.style.background = 'rgba(255, 237, 199, 0.5)';
    }

    function removeColor(node) {
        if (selectedTd) {
            selectedTd.classList.remove('highlight');
        }
        selectedTd = node;
        selectedTd.style.background = 'transparent';
    }

    function returnTarget(event) {
        var target = event.target;
        if (target.tagName !== 'TD')
            return;
        highlight(target);
    }

    function context(event){
        event.preventDefault();
        var target = event.target;
        if (target.tagName !== 'TD')
            return;
        removeColor(target);
    }
    //Создание функции реакций
    createText('Функції реакцій\n', 'title', reactionFnContainer);
    createText('Player1', 'text', reactionFnContainer);
    var functionTablePlayer1 = createReactionFunctionPl1(strategyPlayer1, strategyPlayer2, matrixPlayer1);
    reactionFnContainer.appendChild(functionTablePlayer1);
    //Закрасить левой кнопкой мыши
    functionTablePlayer1.onclick = function(event) {
        returnTarget(event);
    };
    //Вернуть назад правой кнопкой мыши
    functionTablePlayer1.oncontextmenu = function (event) {
        context(event);
    };
    createText('Player2', 'text', reactionFnContainer);
    var functionTablePlayer2 = createReactionFunctionPl2(strategyPlayer1, strategyPlayer2, matrixPlayer2);
    reactionFnContainer.appendChild(functionTablePlayer2);
    functionTablePlayer2.onclick = function(event) {
        returnTarget(event);
    };
    functionTablePlayer2.oncontextmenu = function (event) {
        context(event);
    };

    function solveReactionFunction() {
        createText('Автоматичне розв\'язання\n', 'title', resultFnContainer);
        createText('Player1', 'text', resultFnContainer);
        var solvedFunctionTablePlayer1 = createReactionFunctionPl1(strategyPlayer1, strategyPlayer2, matrixPlayer1);
        resultFnContainer.appendChild(solvedFunctionTablePlayer1);
        createText('Player2', 'text', resultFnContainer);
        var solvedFunctionTablePlayer2 = createReactionFunctionPl2(strategyPlayer1, strategyPlayer2, matrixPlayer2);
        resultFnContainer.appendChild(solvedFunctionTablePlayer2);
        var maxArrayPl1 = [];
        var maxArrayPl2 = [];
        var maxLetter = [];
        var maxPl1, maxPl2;
        for (var i = 0; i < strategyPlayer1; i++) {
            maxPl1 = 0;
            for (var j = 0; j < strategyPlayer1; j++) {
                if(matrixPlayer1[j][i] > maxPl1){
                    maxPl1 = matrixPlayer1[j][i];
                    maxLetter[i] = letter[j];
                }
            }
            maxArrayPl1[i] = maxPl1;
        }
        for (var i = 0; i < strategyPlayer1; i++) {
            for (var j = 0; j < strategyPlayer1; j++) {
                if(matrixPlayer1[i][j] === maxArrayPl1[j]){
                    solvedFunctionTablePlayer1.rows[i].cells[j].style.background = 'rgba(41, 255, 14, 0.5)';
                }
            }
        }
        for (var i = 0; i < strategyPlayer2; i++) {
            maxPl2 = 0;
            for (var j = 0; j < strategyPlayer2; j++) {
                if(matrixPlayer2[i][j] > maxPl2){
                    maxPl2 = matrixPlayer2[i][j];
                }
            }
            maxArrayPl2[i] = maxPl2;
        }
        for (var i = 0; i < strategyPlayer2; i++) {
            for (var j = 0; j < strategyPlayer2; j++) {
                if(matrixPlayer2[i][j] === maxArrayPl2[i]){
                    solvedFunctionTablePlayer2.rows[i].cells[j].style.background = 'rgba(41, 255, 14, 0.5)';
                }
            }
        }
        createText('Формули\n', 'title', formulaContainer);
        createText('Player1', 'text', formulaPl1);
        for(var i = 0; i < maxArrayPl1.length; i++){
            var form = document.createElement('span');
            formulaPl1.appendChild(form);
            form.classList.add('formula');
            form.innerText = "\n˅\nS(" + letter[i] + ") = " + maxLetter[i] + ";\n˅\nU(" + letter[i] + ") = " + maxArrayPl1[i] + ';';
        }

        createText('Player2', 'text', formulaPl2);
        for(var i = 0; i < maxArrayPl2.length; i++) {
            var form2 = document.createElement('span');
            formulaPl2.appendChild(form2);
            form2.classList.add('formula');
            form2.innerText = "\n˅\nS(" + letter[i] + ") = " + maxLetter[i] + ";\n˅\nU(" + letter[i] + ") = " + maxArrayPl2[i] + ';';
        }
        var image = document.createElement('img');
        image.src = 'images/star_butterfly_and_her_wet_socks_by_dorksstar-da1tzkd.png';
        imageContainer.appendChild(image);
        image.classList.add('image');

    }
    solveFunctionBtn.addEventListener('click', solveReactionFunction);

}
function createBinaryMatrix() {
    matrixContainer.innerHTML = '';
    formulaContainer.innerHTML = '';
    binaryMatrixBtn.disabled = true;
    createText('Player1', 'text', binaryMatrixPl1);
    var binaryFunctionTablePlayer1 = createBinaryMatrixPlayer1(strategyPlayer1, strategyPlayer2, matrixPlayer1);
    binaryMatrixPl1.appendChild(binaryFunctionTablePlayer1);
    createText('Player2', 'text', binaryMatrixPl2);
    var binaryFunctionTablePlayer2 = createBinaryMatrixPlayer2(strategyPlayer1, strategyPlayer2, matrixPlayer2);
    binaryMatrixPl2.appendChild(binaryFunctionTablePlayer2);
    stopBtn.onclick = function () {
        for(var i = 0; i < strategyPlayer1; i++)
            btnPlayer1[i].disabled = true;
        for(var i = 0; i < strategyPlayer2; i++)
            btnPlayer2[i].disabled = true;
    };
    var count = 1;
    function deleteColumn(value, matrix, container, table) {
        for (var i = 0; i < table.rows.length; i++) {
            for (var j = 0; j < table.rows[i].cells.length; j++) {
                if (j == value)
                    table.rows[i].cells[j].style.display = "none";
            }
        }
        createText(count + ' хід (Видалена стратегія Player2 - ' + btnPlayer2[value].innerText + ')', 'text', container);
        var newTable =  createNewBinaryTable(strategyPlayer1, strategyPlayer2, matrix);
        container.appendChild(newTable);
        count++;
        var color = 'rgba(' + randomColor() + ',' + randomColor() + ',' + randomColor() + ',0.7)';
        for (var i = 0; i < newTable.rows.length; i++) {
            for (var j = 0; j < newTable.rows[i].cells.length; j++) {
                if (j == value)
                    newTable.rows[i].cells[j].style.background = color;
            }
        }
    }

    //createText("Історія\n", 'title');
    function deleteRow(i, row, container, matrix, table) {
        var index = row.parentNode.parentNode.rowIndex;
        createText(count + ' хід (Видалена стратегія Player1 - ' + btnPlayer1[i].innerText + ')', 'text', container);
        var newTable =  createNewBinaryTable(strategyPlayer1, strategyPlayer2, matrix);
        container.appendChild(newTable);
        newTable.rows[i].style.background = 'rgba(' + randomColor() + ',' + randomColor() + ',' + randomColor() + ',0.7)';
        table.deleteRow(index);
        count++;
    }

    function deleteElement(id, value, container, matrix, table) {
        document.getElementById(id).onclick = function () {
            deleteRow(value, this, container, matrix, table);
        };
    }

    function deleteElementColumn(id, value, matrix, container, table) {
        document.getElementById(id).onclick = function () {
            deleteColumn(value, matrix, container, table);
        };
    }
    deleteElement('btnBinaryPlayerFirst0', 0, binaryMatrixPl1, matrixPlayer1, binaryFunctionTablePlayer1);
    deleteElementColumn('btnBinaryPlayerSecond0', 0, matrixPlayer2, binaryMatrixPl1, binaryFunctionTablePlayer1);
    deleteElement('btnBinary2PlayerFirst0', 0, binaryMatrixPl2, matrixPlayer1, binaryFunctionTablePlayer2);
    deleteElementColumn('btnBinary2PlayerSecond0', 0, matrixPlayer2, binaryMatrixPl2, binaryFunctionTablePlayer2);

    deleteElement('btnBinaryPlayerFirst1', 1, binaryMatrixPl1, matrixPlayer1, binaryFunctionTablePlayer1);
    deleteElementColumn('btnBinaryPlayerSecond1', 1, matrixPlayer2, binaryMatrixPl1, binaryFunctionTablePlayer1);
    deleteElement('btnBinary2PlayerFirst1', 1, binaryMatrixPl2, matrixPlayer1, binaryFunctionTablePlayer2);
    deleteElementColumn('btnBinary2PlayerSecond1', 1, matrixPlayer2, binaryMatrixPl2, binaryFunctionTablePlayer2);

    deleteElement('btnBinaryPlayerFirst2', 2, binaryMatrixPl1, matrixPlayer1, binaryFunctionTablePlayer1);
    deleteElementColumn('btnBinaryPlayerSecond2', 2, matrixPlayer2, binaryMatrixPl1, binaryFunctionTablePlayer1);
    deleteElement('btnBinary2PlayerFirst2', 2, binaryMatrixPl2, matrixPlayer1, binaryFunctionTablePlayer2);
    deleteElementColumn('btnBinary2PlayerSecond2', 2, matrixPlayer2, binaryMatrixPl2, binaryFunctionTablePlayer2);

    deleteElement('btnBinaryPlayerFirst3', 3, binaryMatrixPl1, matrixPlayer1, binaryFunctionTablePlayer1);
    deleteElementColumn('btnBinaryPlayerSecond3', 3, matrixPlayer2, binaryMatrixPl1, binaryFunctionTablePlayer1);
    deleteElement('btnBinary2PlayerFirst3', 3, binaryMatrixPl2, matrixPlayer1, binaryFunctionTablePlayer2);
    deleteElementColumn('btnBinary2PlayerSecond3', 3, matrixPlayer2, binaryMatrixPl2, binaryFunctionTablePlayer2);

    deleteElement('btnBinaryPlayerFirst4', 4, binaryMatrixPl1, matrixPlayer1, binaryFunctionTablePlayer1);
    deleteElementColumn('btnBinaryPlayerSecond4', 4, matrixPlayer2, binaryMatrixPl1, binaryFunctionTablePlayer1);
    deleteElement('btnBinary2PlayerFirst4', 4, binaryMatrixPl2, matrixPlayer1, binaryFunctionTablePlayer2);
    deleteElementColumn('btnBinary2PlayerSecond4', 4, matrixPlayer2, binaryMatrixPl2, binaryFunctionTablePlayer2);

    deleteElement('btnBinaryPlayerFirst5', 5, binaryMatrixPl1, matrixPlayer1, binaryFunctionTablePlayer1);
    deleteElementColumn('btnBinaryPlayerSecond5', 5, matrixPlayer2, binaryMatrixPl1, binaryFunctionTablePlayer1);
    deleteElement('btnBinary2PlayerFirst5', 5, binaryMatrixPl2, matrixPlayer1, binaryFunctionTablePlayer2);
    deleteElementColumn('btnBinary2PlayerSecond5', 5, matrixPlayer2, binaryMatrixPl2, binaryFunctionTablePlayer2);

    deleteElement('btnBinaryPlayerFirst6', 6, binaryMatrixPl1, matrixPlayer1, binaryFunctionTablePlayer1);
    deleteElementColumn('btnBinaryPlayerSecond6', 6, matrixPlayer2, binaryMatrixPl1, binaryFunctionTablePlayer1);
    deleteElement('btnBinary2PlayerFirst6', 6, binaryMatrixPl2, matrixPlayer1, binaryFunctionTablePlayer2);
    deleteElementColumn('btnBinary2PlayerSecond6', 6, matrixPlayer2, binaryMatrixPl2, binaryFunctionTablePlayer2);

    deleteElement('btnBinaryPlayerFirst7', 7, binaryMatrixPl1, matrixPlayer1, binaryFunctionTablePlayer1);
    deleteElementColumn('btnBinaryPlayerSecond7', 7, matrixPlayer2, binaryMatrixPl1, binaryFunctionTablePlayer1);
    deleteElement('btnBinary2PlayerFirst7', 7, binaryMatrixPl2, matrixPlayer1, binaryFunctionTablePlayer2);
    deleteElementColumn('btnBinary2PlayerSecond7', 7, matrixPlayer2, binaryMatrixPl2, binaryFunctionTablePlayer2);

    deleteElement('btnBinaryPlayerFirst8', 8, binaryMatrixPl1, matrixPlayer1, binaryFunctionTablePlayer1);
    deleteElementColumn('btnBinaryPlayerSecond8', 8, matrixPlayer2, binaryMatrixPl1, binaryFunctionTablePlayer1);
    deleteElement('btnBinary2PlayerFirst8', 8, binaryMatrixPl2, matrixPlayer1, binaryFunctionTablePlayer2);
    deleteElementColumn('btnBinary2PlayerSecond8', 8, matrixPlayer2, binaryMatrixPl2, binaryFunctionTablePlayer2);

    deleteElement('btnBinaryPlayerFirst9', 9, binaryMatrixPl1, matrixPlayer1, binaryFunctionTablePlayer1);
    deleteElementColumn('btnBinaryPlayerSecond9', 9, matrixPlayer2, binaryMatrixPl1, binaryFunctionTablePlayer1);
    deleteElement('btnBinary2PlayerFirst9', 9, binaryMatrixPl2, matrixPlayer1, binaryFunctionTablePlayer2);
    deleteElementColumn('btnBinary2PlayerSecond9', 9, matrixPlayer2, binaryMatrixPl2, binaryFunctionTablePlayer2);


}

generateBtn.addEventListener('click', createMatrixBtn);
